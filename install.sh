#!/bin/sh

check_and_create_ssh_key () {
        if [ -f ~/.ssh/id_rsa.pub ]; then
                echo "File exist."
        else
                echo "File doest not exist."
                echo -e "\n" | ssh-keygen -t rsa -N ""
        fi
        echo $'\nPlease register ssh key to git server\n'
        echo "======================================"
        cat ~/.ssh/id_rsa.pub
        echo "======================================"
}

git_clone_jbbox () {

        if ! yum list installed git >/dev/null 2>&1; then
                sudo yum install git -y
        fi
        
        cd ~
        mkdir -p jbdesk/bin
        cd jbdesk/bin
        git clone git@gitlab.com:jbdesk/src/script/bash/jbdesk/jbbox.git
        find jbbox/ -type f -iname "*.sh" -exec chmod +x {} \;
        sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
        sudo yum update -y
        source ~/.bashrc
}

print_menu () {
        echo "1) check and create ssh key"
        echo "2) git clone jbbox"
        echo "3) Quit"
}

PS3='Please enter your choice: '
options=("check and create ssh key" "git clone jbbox" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "check and create ssh key")
            echo "check and create ssh key"
            check_and_create_ssh_key
            print_menu
            ;;
        "git clone jbbox")
            echo "git clone jbbox"
            git_clone_jbbox
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done