# 매뉴얼 URL
https://gitlab.com/jbdesk/src/script/bash/jbdesk/jbbox_install

# install jbbox
sh -c "$(curl -s https://gitlab.com/jbdesk/src/script/bash/jbdesk/jbbox_install/-/raw/master/install.sh)"

# git clone
git : git clone git@gitlab.com:jbdesk/src/script/bash/jbdesk/jbbox.git


########################
#folder
########################

##home
~/jbdesk/bin/jbbox

##library
.lib/

##source
.src/

##docker
.docker/